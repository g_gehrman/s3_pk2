#ifndef CDSTART_H
#define CDSTART_H 

#include "Command.h"
#include "CdPlayer.h"

namespace Fh_Pk2_Commands
{
	class CdStart : public Command
    {
        private:
        	Fh_Pk2_Devices::CdPlayer player;

        public:

         	CdStart( Fh_Pk2_Devices::CdPlayer player)
	        {
	            this->player = player;
	        }

	        void execute() override
	        {
				player.start();
	        }
    };

}
#endif
