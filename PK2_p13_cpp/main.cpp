
#include "GaragentorHoch.h"
#include "GaragentorRunter.h"
#include "CdStopp.h"
#include "CdStart.h"
#include "RemoteControl.h"
#include "Command.h"

int main()
{
	Fh_Pk2_Devices::CdPlayer player1 = new Fh_Pk2_Devices::CdPlayer();
    Fh_Pk2_Devices::Garagentor garagentor1 = new Fh_Pk2_Devices::Garagentor();

    Fh_Pk2_Commands::CdStart cdstart = new Fh_Pk2_Commands::CdStart(player1);
    Fh_Pk2_Commands::CdStopp cdstopp = new Fh_Pk2_Commands::CdStopp(player1);

    Fh_Pk2_Commands::GaragentorHoch garhoch = new Fh_Pk2_Commands::GaragentorHoch(garagentor1);
    Fh_Pk2_Commands::GaragentorRunter garrunter = new Fh_Pk2_Commands::GaragentorRunter(garagentor1);

    Fh_Pk2_Rc::RemoteControl rc = new Fh_Pk2_Rc::RemoteControl();

    rc.setCommand(0, cdstart, cdstopp);
    rc.setCommand(1, garhoch, garrunter);

    rc.pressOn(0);
    rc.pressOn(1);
    rc.pressOff(0);
    rc.pressOff(1);

}