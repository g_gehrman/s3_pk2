
#ifndef REMOTECONTROL_H
#define REMOTECONTROL_H

#include "Command.h"

namespace Fh_Pk2_Rc
{
	class RemoteControl
	{
	public:
		Command  tastenOn[] = new Command[4];
		Command  tastenOff[] = new Command[4];

		void setCommand(int i, Command on, Command off)
		{
			 tastenOn[i] = on;
			 tastenOff[i] = off;
		}
		void pressOn(int i)
		{
			tastenOn[i].Execute();
		}
		void pressOff(int i)
		{
			tastenOff[i].Execute();
		};
}

#endif 