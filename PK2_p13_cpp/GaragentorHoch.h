#ifndef GARAGENTORHOCH_H
#define GARAGENTORHOCH_H 

#include "Command.h"
#include "Garagentor.h"

namespace Fh_Pk2_Commands
{
	class GaragentorHoch : public Command
    {
        private:
        	Fh_Pk2_Devices::Garagentor garagentor;

        public:

         	CdStopp( Fh_Pk2_Devices::Garagentor garagentor)
	        {
	            this->garagentor = garagentor;
	        }

	        void execute() override
	        {
				garagentor.hoch();
	        }
    };
}

#endif
