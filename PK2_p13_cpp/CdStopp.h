#ifndef CDSTOPP_H
#define CDSTOPP_H 

#include "Command.h"
#include "CdPlayer.h"

namespace Fh_Pk2_Commands
{
	class CdStopp : public Command
    {
        private:
        	Fh_Pk2_Devices::CdPlayer player;

        public:

         	CdStopp( Fh_Pk2_Devices::CdPlayer player)
	        {
	            this->player = player;
	        }

	        void execute() override
	        {
				player.stopp();
	        }
    };

}

#endif
