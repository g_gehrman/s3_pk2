﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fh.Pk2.Rc_if;
using Fh.Pk2.Devices_if;

namespace Fh.Pk2.Commands_if

{
    class CdStart : Command
    {
        private CdPlayer player;

        public CdStart( CdPlayer player)
        {
            this.player = player;
        }

        public void Execute()
        {
			player.Start();
        }
    }

    class CdStopp: Command
    {
        private CdPlayer player;

        public CdStopp(CdPlayer player)
        {
            this.player = player;
        }
        public void Execute()
        {
            player.Stopp();
        }
    }

	class GaragentorHoch : Command
    {
        private Garagentor garage;

        public GaragentorHoch(Garagentor garage)
        {
            this.garage = garage;
        }

        public void Execute()
        {
            garage.Hoch();
        }
    }

    class GaragentorRunter : Command
    {
        private Garagentor garage;

        public GaragentorRunter(Garagentor garage)
        {
            this.garage = garage;
        }
        public void Execute()
        {
            garage.Runter();
        }
    }
}
