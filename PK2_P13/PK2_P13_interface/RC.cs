﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fh.Pk2.Rc_if
{
    class RemoteControl
    {

        Command[] tastenOn =  new Command[4];
        Command[] tastenOff = new Command[4];

        public void SetCommand(int i, Command on, Command off)
        {
            tastenOn[i] = on;
            tastenOff[i] = off;
        }


        public void PressOn( int i )
        {
            tastenOn[i].Execute();
        }

        public void PressOff( int i )
        {
            tastenOff[i].Execute();
        }

    }




    public interface Command
    {

       void Execute();

    }
}
