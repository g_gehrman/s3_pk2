﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fh.Pk2.Rc;
using Fh.Pk2.Devices;

namespace Fh.Pk2.Commands

{
    class CdStart : Command
    {
        private CdPlayer player;

        public CdStart( CdPlayer player)
        {
            this.player = player;
        }

        public override void Execute()
        {
			player.Start();
        }
    }

    class CdStopp: Command
    {
        private CdPlayer player;

        public CdStopp(CdPlayer player)
        {
            this.player = player;
        }
        public override void Execute()
        {
            player.Stopp();
        }
    }

	class GaragentorHoch : Command
    {
        private Garagentor garage;

        public GaragentorHoch(Garagentor garage)
        {
            this.garage = garage;
        }

        public override void Execute()
        {
            garage.Hoch();
        }
    }

    class GaragentorRunter : Command
    {
        private Garagentor garage;

        public GaragentorRunter(Garagentor garage)
        {
            this.garage = garage;
        }
        public override void Execute()
        {
            garage.Runter();
        }
    }
}
