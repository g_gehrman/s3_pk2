﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fh.Pk2.Commands;
using Fh.Pk2.Rc;
using Fh.Pk2.Devices;

namespace Praktikum13
{
    class Program
    {
        static void Main(string[] args)
        {

            CdPlayer player1 = new CdPlayer();
            Garagentor garagentor1 = new Garagentor();

            CdStart cdstart = new CdStart(player1);
            CdStopp cdstopp = new CdStopp(player1);

            GaragentorHoch garhoch = new GaragentorHoch(garagentor1);
            GaragentorRunter garrunter = new GaragentorRunter(garagentor1);

            RemoteControl rc = new RemoteControl();

            rc.SetCommand(0, cdstart, cdstopp);
            rc.SetCommand(1, garhoch, garrunter);

            rc.PressOn(0);
            rc.PressOn(1);
            rc.PressOff(0);
            rc.PressOff(1);
            Console.ReadLine();
        }
    }
}
